﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebCotizacion.aspx.cs" Inherits="WebAndenInmobiliario.WebCotizacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="card">
        <div class="card-header">
            <h1>COTIZACION</h1>
        </div>
    </div>
    <asp:MultiView ActiveViewIndex="0" ID="vista" runat="server">
        <asp:View runat="server" ID="listar">
            <div class="card-body">
                <asp:GridView ID="gvCotizacion" runat="server" CssClass="table-responsive" AutoGenerateColumns="False"
                    OnRowDeleting="gvCotizacion_RowDeleting" OnRowCommand="gvCotizacion_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="idCotizacion" HeaderText="Cod.Solicitud" />
                        <asp:BoundField DataField="dni" HeaderText="Dni" />
                        <asp:BoundField DataField="nombres" HeaderText="Nombres" />
                        <asp:BoundField DataField="apellidos" HeaderText="Apellidos" />
                        <asp:BoundField DataField="correo" HeaderText="Correo" />
                        <asp:BoundField DataField="telefono" HeaderText="Telefono" />
                        <asp:BoundField DataField="estado" HeaderText="Estado" />
                        <asp:TemplateField ItemStyle-Width="50px" HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnDelete" runat="server" ForeColor="#0C4375" Text="Delete"
                                    CommandName="Delete" CommandArgument='<%#Eval("idCotizacion")%>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Wrap="True" />
                            <ControlStyle BackColor="#EAF6FE" />
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="50px" HeaderText="Editar">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" ForeColor="#0C4375" Text="Editar"
                                    CommandName="Editar" CommandArgument='<%#Eval("idCotizacion")%>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Wrap="True" />
                            <ControlStyle BackColor="#EAF6FE" />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
        <asp:View runat="server" ID="actualizar">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <input runat="server" name="idCotizacion" id="idCotizacion" class="form-control" readonly="readonly" placeholder="Codigo" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input type="text" runat="server" name="dni" id="dni" class="form-control" placeholder="dni" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input type="text" runat="server" name="dni" id="nombres" class="form-control" placeholder="nombres" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input type="text" runat="server" name="apellidos" id="apellidos" class="form-control" placeholder="apellidos" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input type="email" runat="server" name="correo" id="correo" class="form-control" placeholder="correo" />
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <input type="text" runat="server" name="correo" id="telefono" class="form-control" placeholder="telefono" />
                </div>
            </div>
            <div class="actions">
                <button class="btn btn-info" runat="server" onserverclick="grabar_ServerClick">Grabar</button>
            </div>
        </asp:View>
    </asp:MultiView>

</asp:Content>

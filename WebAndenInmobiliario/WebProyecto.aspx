﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProyecto.aspx.cs" Inherits="WebAndenInmobiliario.WebProyecto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="card">
        <div class="card-header">
            <h1>Proyectos Inmmobiliarios</h1>
        </div>
    </div>
    <asp:MultiView ActiveViewIndex="0" ID="vista" runat="server">
        <asp:View runat="server" ID="listar">
            <div class="card-body">
                <asp:GridView ID="gvProyectos" runat="server" CssClass="table-responsive" AutoGenerateColumns="False"
                    OnRowDeleting="gvProyectos_RowDeleting" OnRowCommand="gvProyectos_RowCommand" >
                    <Columns>
                        <asp:BoundField DataField="IdProyecto" HeaderText="Cod.Proyecto" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                        <asp:BoundField DataField="Direccion" HeaderText="Dirección" />
                        <asp:BoundField DataField="Latitud" HeaderText="Latitud" />
                        <asp:BoundField DataField="Longitud" HeaderText="Longitud" />
                        <asp:TemplateField ItemStyle-Width="50px" HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnDelete" runat="server" ForeColor="#0C4375" Text="Delete"
                                    CommandName="Delete" CommandArgument='<%#Eval("IdProyecto")%>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Wrap="True" />
                            <ControlStyle BackColor="#EAF6FE" />
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="50px" HeaderText="Editar">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" ForeColor="#0C4375" Text="Editar"
                                    CommandName="Editar" CommandArgument='<%#Eval("IdProyecto")%>'
                                    >
                                </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Wrap="True" />
                            <ControlStyle BackColor="#EAF6FE" />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
                <asp:LinkButton runat="server" CssClass="btn btn-success" OnClick="nuevo_Click">Nuevo Proyecto</asp:LinkButton>
            </div>
        </asp:View>
        <asp:View runat="server" ID="actualizar">
              <div class="form-row">
                <div class="form-group col-md-6">
                    <input runat="server" name="codigo" id="proyect_codigo" class="form-control" readonly="readonly" placeholder="Codigo" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input runat="server" name="nombre" id="proyect_nombre" class="form-control" placeholder="Nombre" />
                </div>
            </div>


            <div class="form-row">
                <div class="form-group col-md-6">
                    <input runat="server" name="direccion" id="proyect_direccion" class="form-control" placeholder="Direccion" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input runat="server" name="latitud" id="proyect_latitud" class="form-control" placeholder="Latitud" />
                </div>
            </div>


            <div class="form-row">
                <div class="form-group col-md-6">
                    <input runat="server" name="longitud" id="proyect_longitud" class="form-control" placeholder="Longitud" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input runat="server" name="precio" id="proyect_precio" class="form-control" placeholder="Precio" />
                </div>
            </div>


            <div class="form-row">
                <div class="form-group col-md-6">
                    <asp:Label runat="server" ID="activo" Text="activo"></asp:Label>
                    <asp:CheckBox ID="ckBactivo" runat="server" />
                </div>
            </div>
            <div class="actions">
                <button class="btn btn-info" runat="server" onserverclick="grabar_ServerClick">Grabar</button>
            </div>
        </asp:View>
    </asp:MultiView>

</asp:Content>

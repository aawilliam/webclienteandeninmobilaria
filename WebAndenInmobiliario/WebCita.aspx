﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebCita.aspx.cs" Inherits="WebAndenInmobiliario.WebCita" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="card">
        <div class="card-header">
            <h1>CITAS</h1>
        </div>
    </div>
    <asp:MultiView ActiveViewIndex="0" ID="vista" runat="server">
        <asp:View runat="server" ID="listar">
            <div class="card-body">
                <asp:GridView ID="gvCitas" runat="server" CssClass="table-responsive" AutoGenerateColumns="False"
                    OnRowDeleting="gvCitas_RowDeleting" OnRowCommand="gvCitas_RowCommand" >
                    <Columns>
                        <asp:BoundField DataField="idCita" HeaderText="Cod.Cita" />
                        <asp:BoundField DataField="fechaCita" HeaderText="Fecha" />
                        <asp:BoundField DataField="idSolicitud" HeaderText="Solicitud" />
                        <asp:BoundField DataField="idUsuario" HeaderText="Usuario" />
                        <asp:BoundField DataField="estado" HeaderText="estado" />
                        <asp:TemplateField ItemStyle-Width="50px" HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnDelete" runat="server" ForeColor="#0C4375" Text="Delete"
                                    CommandName="Delete" CommandArgument='<%#Eval("idCita")%>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Wrap="True" />
                            <ControlStyle BackColor="#EAF6FE" />
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="50px" HeaderText="Editar">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" ForeColor="#0C4375" Text="Editar"
                                    CommandName="Editar" CommandArgument='<%#Eval("idCita")%>'
                                    >
                                </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Wrap="True" />
                            <ControlStyle BackColor="#EAF6FE" />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
                <asp:LinkButton runat="server" CssClass="btn btn-success" OnClick="nuevo_Click">Nueva Cita</asp:LinkButton>
            </div>
        </asp:View>
        <asp:View runat="server" ID="actualizar">
              <div class="form-row">
                <div class="form-group col-md-6">
                    <input runat="server" name="idCita" id="idCita" class="form-control" readonly="readonly" placeholder="Codigo" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input type="date" runat="server" name="fechaCita" id="fechaCita" class="form-control" />
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Solicitud:</label>
                    <asp:DropDownList ID="cboSolicitud" runat="server"></asp:DropDownList>
                </div>
            </div>
            <div class="actions">
                <button class="btn btn-info" runat="server" onserverclick="grabar_ServerClick">Grabar</button>
            </div>
        </asp:View>
    </asp:MultiView>

</asp:Content>

﻿using CapaEntidad;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAndenInmobiliario
{
    public partial class Index : System.Web.UI.Page
    {
        //CapaDatos.CotizacionDAO wcfDatos = null;
        string urlServicio = WebConfigurationManager.AppSettings["url_cotizacion"];
        string url_mensaje = WebConfigurationManager.AppSettings["url_mensaje"];
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void LimpiarControles()
        {
            txtDni.Text = "";
            txtNombres2.Value = "";
            txtApellido.Value = "";
            txtEmail.Value = "";
            txtTelefono.Value = "";
        }


        private void RegistrarCotizacion()
        {
            try
            {

                CotizacionDominio cotizacion = new CotizacionDominio();
                cotizacion.Dni = txtDni.Text;
                cotizacion.Nombres = txtNombres2.Value;
                cotizacion.Apellidos = txtApellido.Value;
                cotizacion.Correo = txtEmail.Value;
                cotizacion.Telefono = txtTelefono.Value;
                cotizacion.Estado = true;
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Cotizacion");
                request.Method = Method.POST;
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddBody(cotizacion);
                IRestResponse<CotizacionDominio> response = client.Execute<CotizacionDominio>(request);

                if (response.ErrorException != null)
                {
                    string message = response.Content.ToString();                 
                    string myStringVariable = message;
                    ClientScript.RegisterStartupScript(this.GetType(), "ERROR", "alert('" + myStringVariable + "');", true);
                }
                else
                {
                    string texto = "Hola "+cotizacion.Nombres+" se ha registrado su cotizacion en breve le reponderemos al correo "+cotizacion.Correo;
                    var msj_sms = new RestClient(url_mensaje);
                    var request_msj = new RestRequest("Mensaje/"+ texto+"/"+ cotizacion.Telefono.ToString());
                    request_msj.Method = Method.GET;
                    IRestResponse<MensajeDominio> response_msj = msj_sms.Execute<MensajeDominio>(request_msj);

                    string myStringVariable = "Tu solicitud ha sido registrado de forma correcta";
                    ClientScript.RegisterStartupScript(this.GetType(), "INFORMACION", "alert('" + myStringVariable + "');", true);
                }
                LimpiarControles();
            }
            catch (Exception ex)
            {
                string myStringVariable = "ERROR: " + ex.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "ERROR", "alert('" + myStringVariable + "');", true);
            }


        }

        protected void cfsubmit_ServerClick(object sender, EventArgs e)
        {
            RegistrarCotizacion();
        }




    }
}
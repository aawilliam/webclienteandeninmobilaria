﻿using CapaEntidad;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAndenInmobiliario
{
    public partial class Login : System.Web.UI.Page
    {
        string urlServicio = WebConfigurationManager.AppSettings["url"];
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void Logeo() {

           
            LoginDominio login = new LoginDominio();
            login.userName = email.Value.Trim();
            login.password = password.Value.Trim();            
            var client = new RestClient(urlServicio);
            var request = new RestRequest("Login");

            var logeo = SimpleJson.SerializeObject(login);
            request.Method = Method.POST;
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();            
            request.AddBody(new { username = login.userName, password=login.password });
            IRestResponse<UsuarioDominio> response2 = client.Execute<UsuarioDominio>(request);
            Session["Nombre"] = response2.Data.Nombre;
            Session["IdUsuario"] = response2.Data.IdUsuario;
            Session["IdRol"] = response2.Data.IdRol;
            Session["Email"] = response2.Data.Email;
            Session["Dni"] = response2.Data.Dni;
            Session["Apellidos"] = response2.Data.Apellidos;
            Response.Redirect("~/WebRol.aspx");

        }

        protected void logeo_ServerClick(object sender, EventArgs e)
        {
            this.Logeo();
        }
    }
}
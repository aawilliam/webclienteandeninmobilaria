﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WebAndenInmobiliario.Index" %>

<!DOCTYPE html>

<html>
<head runat="server">

    <title>AndenInmobiliaria</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/custom.js"></script>
    <%--<script src="js/cable.js"></script>--%>

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/owl.carousel.css" rel="stylesheet" />
    <link href="css/owl.theme.default.min.css" rel="stylesheet" />
    <link href="css/magnific-popup.css" rel="stylesheet" />
    <link href="css/templatemo-style.css" rel="stylesheet" />
    <link href="css/estilo.css" rel="stylesheet" />



</head>
<body>

    <form runat="server">

    

        <section class="preloader">
            <div class="spinner">

                <span class="spinner-rotate"></span>

            </div>
        </section>


        <!-- MENU -->
        <section class="navbar custom-navbar navbar-fixed-top cabecera" role="navigation">
            <div class="container ">

                <div class="navbar-header ">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon icon-bar"></span>
                        <span class="icon icon-bar"></span>
                        <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="index.aspx" class="navbar-brand " />
                        <img src="Images/logo.png" />
                </div>

                <!-- MENU LINKS -->
                <div class="collapse navbar-collapse ">
                    <ul class="nav navbar-nav navbar-nav-first ">
                        <li><a href="#home"></a></li>
                        <li><a href="#home">Inicio</a></li>
                        <li><a href="#about">Nosotros</a></li>
                        <li><a href="#team">Proyectos</a></li>
                        <li><a href="#menu">Galería</a></li>
                        <li>
                            <a href="#contact" class="smoothScroll">Cotizar</a>

                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <!--<li><a href="#">Llámenos! <i class="fa fa-phone"></i> 970565541</a></li>-->
                        <a href="Login.aspx" class="smoothScroll">Ingresar</a>
                    </ul>
                </div>

            </div>
        </section>
        <br />
        <br />
        <br />
        <br />
        <br />

     

        <!-- HOME -->
        <section id="home" class="carousel-item" data-stellar-background-ratio="0.5">
            <div class="row">
                <div class="owl-carousel owl-theme">
                    <div class="item" style="background-image: url(Images/banner-home-01.jpg)">
                        <div class="caption">
                            <div class="container">
                                <div class="col-md-8 col-sm-12">
                                    <br />

                                    <h3>VIVE EN EL LUGAR SOÑADO<</h3>
                                    <h1>Elije un departamento para estar relajado y en familia, tenemos una selecta colección.</h1>
                                    <a href="#team" class="section-btn btn btn-default smoothScroll">Conocer Proyectos</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item" style="background-image: url(Images/banner-home-02.jpg)">
                        <div class="caption">
                            <div class="container">
                                <div class="col-md-8 col-sm-12">
                                    <h3>¿CÓMO ESCOGER TU DEPARTAMENTO IDEAL?</h3>
                                    <h1>Escoger tu primer departamento debe ser divertido y emocionante, estás seleccionando el lugar que será el santuario para tu vida.</h1>
                                    <a href="#menu" class="section-btn btn btn-default smoothScroll">Discover menu</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item" style="background-image: url(Images/banner-home-03.jpg)">
                        <div class="caption">
                            <div class="container">
                                <div class="col-md-8 col-sm-12">
                                    <h3>ADQUIERE HOY TU DEPARTAMENTO</h3>
                                    <h1>Comienza a vivir de una manera distinta en un lugar increíble con la mejor comodidad.proyect/proyect_01.jpg</h1>
                                    <a href="#contact" class="section-btn btn btn-default smoothScroll">Reservation</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


        <!-- ABOUT -->
        <section id="about" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 col-sm-12">
                        <div class="about-info">
                            <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                <h3>¿POR QUÉ CONFIAR EN ANDEN INMOBILIARIA?</h3>
                            </div>

                            <div class="wow fadeInUp" data-wow-delay="0.4s">
                                <p class="description">
                                    13 años al servicio de las familia peruanas<br>
                                    15 proyectos terminados<br />
                                    Con más de 480 departamentos entregados.
                                </p>

                                <h4 class="title">MÁS CERCA DE TODO</h4>
                                <p class="description">Todos nuestros proyectos cuentan con fácil acceso a universidades, colegios, institutos, centros comerciales y avenidas principales.</p>

                                <h4 class="title">CALIDAD</h4>
                                <p class="description">Nuestros departamentos cuentan con un diseño impresionante y acabados de lujo, que seguramente disfrutará de esta impresionante residencia.</p>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                            <img src="Images/nosotros.png" class="img-responsive" alt="" />
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <!-- TEAM -->
        <section id="team" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 col-sm-12">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                            <h2>Proyectos Inmobiliarios</h2>
                            <h4>EN VENTA</h4>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                            <img src="Images/proyect_01.jpg" class="img-responsive" alt="" />

                            <div class="team-hover">
                                <div class="team-item">
                                    <h4>La Torre Orquídeas, un proyecto que cuenta con todo lo necesario para satisfacer las diversas necesidades empresariales</h4>
                                    <ul class="social-icon">
                                        <li><a href="#" class="fa fa-linkedin-square"></a></li>
                                        <li><a href="#" class="fa fa-envelope-o"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="team-info">
                            <h3>Torre Orquídeas</h3>
                            <p>Building with flowers</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class="team-thumb wow fadeInUp" data-wow-delay="0.4s">
                            <img src="Images/proyect_02.jpg" class="img-responsive" alt="" />
                            <div class="team-hover">
                                <div class="team-item">
                                    <h4>La Torre LUX se impone en el límite con San Isidro, para que tomes la ciudad con tus ojos, admirando la vista panorámica y descansando en un espacio de total tranquilidad.</h4>
                                    <ul class="social-icon">
                                        <li><a href="#" class="fa fa-instagram"></a></li>
                                        <li><a href="#" class="fa fa-flickr"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="team-info">
                            <h3>Torre Lux</h3>
                            <p>American Dream</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class="team-thumb wow fadeInUp" data-wow-delay="0.6s">
                            <img src="Images/proyect_03.jpg" class="img-responsive" alt="" />
                            <div class="team-hover">
                                <div class="team-item">
                                    <h4>El centro empresarial Lima Central Tower combina diseño, tecnología y seguridad de vanguardia. Está diseñado teniendo en cuenta la mayor productividad de las empresas.</h4>
                                    <ul class="social-icon">
                                        <li><a href="#" class="fa fa-github"></a></li>
                                        <li><a href="#" class="fa fa-google"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="team-info">
                            <h3>Lima Central Tower</h3>
                            <p>Millenium Desing</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <!-- MENU-->
        <section id="menu" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 col-sm-12">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                            <h2>Más de nuestros proyectos</h2>
                            <h4>Edificios Modernos</h4>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <!-- MENU THUMB -->
                        <div class="menu-thumb">

                            <a href="Images/proyect_01.jpg" class="image-popup" title="American Breakfast">
                                <img src="Images/proyect_01.jpg" class="img-responsive" alt="" />

                                <div class="menu-info">
                                    <div class="menu-item">
                                        <h3>Torre Barlovento</h3>
                                        <p>4 Dormitorios / Sala de entretenimiento / Comedor</p>
                                    </div>
                                    <div class="menu-price">
                                        <span>$350,000</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <!-- MENU THUMB -->
                        <div class="menu-thumb">
                            <a href="Images/proyect_02.jpg" class="image-popup" title="Self-made Salad">
                                <img src="Images/proyect_02.jpg" class="img-responsive" alt="" />

                                <div class="menu-info">
                                    <div class="menu-item">
                                        <h3>Torre Wiese</h3>
                                        <p>Jardin / Comedor / Cochera</p>
                                    </div>
                                    <div class="menu-price">
                                        <span>$220,000</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <!-- MENU THUMB -->
                        <div class="menu-thumb">
                            <a href="Images/proyect_03.jpg" class="image-popup" title="Chinese Noodle">
                                <img src="Images/proyect_03.jpg" class="img-responsive" alt="" />

                                <div class="menu-info">
                                    <div class="menu-item">
                                        <h3>Edificio Javier Alzamora Valdez</h3>
                                        <p>Comedor / 3 Dormitorios / Sala de entrenimiento</p>
                                    </div>
                                    <div class="menu-price">
                                        <span>$340,000</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <!-- MENU THUMB -->
                        <div class="menu-thumb">
                            <a href="Images/proyect_04.jpg" class="image-popup" title="Rice Soup">
                                <img src="Images/proyect_04.jpg" class="img-responsive" alt="" />

                                <div class="menu-info">
                                    <div class="menu-item">
                                        <h3>Edificio La Colmena</h3>
                                        <p>Jardin / Comedor / 2 Baños</p>
                                    </div>
                                    <div class="menu-price">
                                        <span>$280,000</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <!-- MENU THUMB -->
                        <div class="menu-thumb">
                            <a href="Images/proyect_05.jpg" class="image-popup" title="Project title">
                                <img src="Images/proyect_05.jpg" class="img-responsive" alt="" />

                                <div class="menu-info">
                                    <div class="menu-item">
                                        <h3>Edificio Capital     </h3>
                                        <p>Comedor / 3 Dormitorios / 2 Baños</p>
                                    </div>
                                    <div class="menu-price">
                                        <span>$260,000</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <!-- MENU THUMB -->
                        <div class="menu-thumb">
                            <a href="Images/proyect_06.jpg" class="image-popup" title="Project title">
                                <img src="Images/proyect_06.jpg" class="img-responsive" alt="" />

                                <div class="menu-info">
                                    <div class="menu-item">
                                        <h3>Torre Parque Mar</h3>
                                        <p>Comedor / 2 Dormitorios / 2 Baños</p>
                                    </div>
                                    <div class="menu-price">
                                        <span>$270,000</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>


                </div>
            </div>
        </section>


        <!-- TESTIMONIAL -->
        <section id="testimonial" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">



                    <div class="col-md-offset-2 col-md-8 col-sm-12">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum ullamcorper nulla non.</p>
                                <div class="tst-author">
                                    <h4>Digital Carlson</h4>
                                    <span>Pharetra quam sit amet</span>
                                </div>
                            </div>

                            <div class="item">
                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed vestibulum orci quam.</p>
                                <div class="tst-author">
                                    <h4>Johnny Stephen</h4>
                                    <span>Magna nisi porta ligula</span>
                                </div>
                            </div>

                            <div class="item">
                                <p>Vivamus aliquet felis eu diam ultricies congue. Morbi porta lorem nec consectetur porta quis dui elit habitant morbi.</p>
                                <div class="tst-author">
                                    <h4>Jessie White</h4>
                                    <span>Vitae lacinia augue urna quis</span>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section>


        <!-- CONTACT -->
        <section id="contact" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">
                    <!-- How to change your own map point
            1. Go to Google Maps
            2. Click on your location point
            3. Click "Share" and choose "Embed map" tab
            4. Copy only URL and paste it within the src="" field below
	-->
                    <div class="wow fadeInUp col-md-6 col-sm-12" data-wow-delay="0.4s">
                        <div id="google-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.1181837144877!2d-76.96509068567521!3d-12.104060991429167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c70ab77498c1%3A0xcce9f50642e7c684!2sUPC+Monterrico!5e0!3m2!1ses!2spe!4v1562311629013!5m2!1ses!2spe" width="600" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">

                        <div class="col-md-12 col-sm-12">
                            <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                                <h2>Solicitar Cotización</h2>
                            </div>
                        </div>

                        <!-- CONTACT FORM -->
                        <div class="wow fadeInUp" id="contact-form" data-wow-delay="0.8s">

                            <!-- IF MAIL SENT SUCCESSFUL  // connect this with custom JS -->
                            <h6 class="text-success">Your message has been sent successfully.</h6>

                            <!-- IF MAIL NOT SENT -->
                            <h6 class="text-danger">E-mail must be valid and message must be longer than 1 character.</h6>

                            <div class="col-md-6 col-sm-6">
                                
                                <asp:TextBox ID="txtDni" runat="server" class="form-control" placeholder="Ingrese su DNI" ></asp:TextBox>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <input type="text" class="form-control" runat="server"  id="txtNombres2"  placeholder="Nombre" />
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" class="form-control" runat="server"  id="txtApellido" placeholder="Apellido" />
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <input type="email" class="form-control"  runat="server" id="txtEmail"  placeholder="Email" />
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <input type="text" class="form-control"  runat="server" id="txtTelefono"  placeholder="Telefono" />
                            </div>

                            <div class="col-md-12 col-sm-12">
                                <button type="submit" class="form-control" onserverclick="cfsubmit_ServerClick" runat="server" id="btnsubmit" name="submit">Solicitar Cotización</button>

                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>





        <!-- FOOTER -->
        <footer id="footer" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">

                    <div class="col-md-3 col-sm-8">
                        <div class="footer-info">
                            <div class="section-title">
                                <h2 class="wow fadeInUp" data-wow-delay="0.2s">ANDEN INMOBILIARIA</h2>
                            </div>
                            <address class="wow fadeInUp" data-wow-delay="0.4s">
                                <p>
                                    Todas las imágenes, planos, medidas y áreas, son referenciales por lo que podrían sufrir cambios al momento de desarrollarse el proyecto, asímismo los elementos decorativos, acabados y mobiliarios son propuestas del departamento de diseño que no se incluyen en la oferta comercial y no comprometen a la empresa inmobiliaria.
                                </p>
                            </address>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-8">
                        <div class="footer-info">
                            <div class="section-title">
                                <h2 class="wow fadeInUp" data-wow-delay="0.2s">Reserva tu departamento</h2>
                            </div>
                            <address class="wow fadeInUp" data-wow-delay="0.4s">
                                <p>993580137 | 970565541</p>
                                <p><a href="mailto:info@company.com">info@andeninmobiliaria.com</a></p>
                                <p>Av. La Encalada 1615 - Of. 208 Surco, Lima, Peru</p>
                            </address>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-8">
                        <div class="footer-info footer-open-hour">
                            <div class="section-title">
                                <h2 class="wow fadeInUp" data-wow-delay="0.2s">Horario de atención</h2>
                            </div>
                            <div class="wow fadeInUp" data-wow-delay="0.4s">
                                <div>
                                    <strong>Lunes a Viernes</strong>
                                    <p>7:00 AM - 9:00 PM</p>
                                </div>
                                <div>
                                    <strong>Sábado</strong>
                                    <p>11:00 AM - 10:00 PM</p>
                                </div>
                                <div>
                                    <strong>Domingo</strong>
                                    <p>Cerrado</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-4">
                        <ul class="wow fadeInUp social-icon" data-wow-delay="0.4s">
                            <li><a href="#" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-instagram"></a></li>
                            <li><a href="#" class="fa fa-google"></a></li>
                        </ul>

                        <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s">
                            <p>
                                <br />
                                Copyright &copy; 2019
                                <br>
                                ANDEN INMOBILIARIA 
                              
                              <br />
                                <br />
                                Diseño: <a rel="nofollow" href="http://templatemo.com" target="_parent">Todoinmueble.pe</a>
                                <br />
                                <br />
                                Distribución: <a href="http://themewagon.com" target="_parent">TemaMoon</a>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </footer>







    


    </form>
</body>
</html>

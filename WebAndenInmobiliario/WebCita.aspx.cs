﻿using CapaEntidad;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAndenInmobiliario
{
    public partial class WebCita : System.Web.UI.Page
    {
        string urlServicio = WebConfigurationManager.AppSettings["url_cita"];
        string url_cotizacion = WebConfigurationManager.AppSettings["url_cotizacion"];
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                vista.SetActiveView(listar);
                Listado();
               
            }
        }


        private void ListadoCotizacion()
        {
            var client = new RestClient(url_cotizacion);
            var request = new RestRequest("Cotizacion");
            request.Method = Method.GET;
            IRestResponse<List<CotizacionDominio>> response = client.Execute<List<CotizacionDominio>>(request);
            List<CotizacionDominio> listado = response.Data;
            cboSolicitud.DataSource = listado;
            cboSolicitud.DataTextField = "nombres";
            cboSolicitud.DataValueField = "idCotizacion";
            cboSolicitud.DataBind();
        }


        private void Listado()
        {

            var client = new RestClient(urlServicio);
            var request = new RestRequest("Cita");
            request.Method = Method.GET;
            IRestResponse<List<CitaDominio>> response = client.Execute<List<CitaDominio>>(request);
            List<CitaDominio> listado = response.Data;
            gvCitas.DataSource = listado;
            gvCitas.DataBind();
        }

        protected void gvCitas_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void grabar_ServerClick(object sender, EventArgs e)
        {
            if (idCita.Value.ToString() == "")
            {
                CitaDominio cita = new CitaDominio();
                cita.idCita =0;
                cita.fechaCita = Convert.ToDateTime(fechaCita.Value).ToString("dd/MM/yyyy");
                cita.idSolicitud = Convert.ToInt32(cboSolicitud.SelectedValue);
                cita.idUsuario =Convert.ToInt32(Session["idUsuario"]);
                cita.estado = true;
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Cita");
                request.Method = Method.POST;
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddBody(cita);
                IRestResponse<CitaDominio> response = client.Execute<CitaDominio>(request);
                if (response.ErrorException != null)
                {
                    string message = "ERROR: "+ response.Content.ToString();
                    var twilioException = message;
                    string myStringVariable = message;
                    ClientScript.RegisterStartupScript(this.GetType(), "ERROR", "alert('" + myStringVariable + "');", true);

                }

            }
            else
            {
                CitaDominio cita = new CitaDominio();
                cita.idCita = Convert.ToInt32(idCita.Value.Trim());
                cita.fechaCita = Convert.ToString(fechaCita.Value);
                cita.idSolicitud = Convert.ToInt32(cboSolicitud.SelectedValue);
                cita.idUsuario = Convert.ToInt32(Session["idUsuario"]);
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Cita");
                var logeo = SimpleJson.SerializeObject(cita);
                request.Method = Method.PUT;
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddBody(cita);
                IRestResponse<CitaDominio> response = client.Execute<CitaDominio>(request);
                if (response.ErrorException != null)
                {
                    string message = "ERROR: " + response.Content.ToString();
                    var twilioException = message;
                    string myStringVariable = message;
                    ClientScript.RegisterStartupScript(this.GetType(), "ERROR", "alert('" + myStringVariable + "');", true);
                }
                // throw twilioException;
            }

            this.Listado();
            vista.SetActiveView(listar);
        }

        protected void nuevo_Click(object sender, EventArgs e)
        {
            ListadoCotizacion();
            vista.SetActiveView(actualizar);
        }

        protected void gvCitas_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Delete")
            {

                int idProyecto = Convert.ToInt32(e.CommandArgument.ToString());
                //int prodcutid = Convert.ToInt32(lbtnDelete.CommandArgument.ToString());
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Cita/" + idProyecto.ToString(), Method.DELETE);
                IRestResponse response = client.Execute(request);
                this.Listado();


            }
            else if (e.CommandName == "Editar")
            {
                ListadoCotizacion();
                int idProyecto = Convert.ToInt32(e.CommandArgument.ToString());
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Cita/" + idProyecto.ToString());
                request.Method = Method.GET;
                IRestResponse<CitaDominio> response = client.Execute<CitaDominio>(request);
                CitaDominio obj = response.Data;
                idCita.Value = Convert.ToString(obj.idCita);
                fechaCita.Value = Convert.ToDateTime(obj.fechaCita).ToString("yyyy-MM-dd");
                cboSolicitud.SelectedValue = obj.idSolicitud.ToString();
                
                vista.SetActiveView(actualizar);
            }
        }
    }
}
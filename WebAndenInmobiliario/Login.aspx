﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebAndenInmobiliario.Login" %>

<!DOCTYPE html>
<html>
<head>
    <title>AppProyecto</title>

    <%--    <%= csrf_meta_tags %>
    <%= stylesheet_link_tag    'application', media: 'all', 'data-turbolinks-track': 'reload' %>
    <%= javascript_include_tag 'application', 'data-turbolinks-track': 'reload' %>--%>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        body {
            color: #fff;
            opacity: 0.9;
            background-image: url(Images/background_login.jpg);
            background-size: cover;
        }

        .form-control {
            min-height: 41px;
            background: #fff;
            box-shadow: none !important;
            border-color: #e3e3e3;
        }

            .form-control:focus {
                border-color: #C23C20;
            }

        .form-control, .btn {
            border-radius: 2px;
        }

        .login-form {
            width: 350px;
            margin: 0 auto;
            padding: 100px 0 30px;
        }

            .login-form form {
                color: #7a7a7a;
                border-radius: 2px;
                margin-bottom: 15px;
                font-size: 13px;
                background: #ececec;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
                position: relative;
            }

            .login-form h2 {
                font-size: 22px;
                margin: 35px 0 25px;
            }

            .login-form .avatar {
                position: absolute;
                margin: 0 auto;
                left: 0;
                right: 0;
                top: -50px;
                width: 95px;
                height: 95px;
                border-radius: 50%;
                z-index: 9;
                background: #121117;
                padding: 15px;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
            }

                .login-form .avatar img {
                    width: 100%;
                    margin-top: 25%;
                }

            .login-form input[type="checkbox"] {
                margin-top: 2px;
            }

            .login-form .btn {
                font-size: 16px;
                font-weight: bold;
                background: #CF5829;
                border: none;
                margin-bottom: 20px;
            }

                .login-form .btn:hover, .login-form .btn:focus {
                    background: #D1220D;
                    outline: none !important;
                }

            .login-form a {
                color: #fff;
                text-decoration: underline;
            }

                .login-form a:hover {
                    text-decoration: none;
                }

            .login-form form a {
                color: #7a7a7a;
                text-decoration: none;
            }

                .login-form form a:hover {
                    text-decoration: underline;
                }
    </style>
</head>

<body>
     <form runat="server">
    <div class="login-form" >
        <div class="avatar avatars">
            <img src="Images/logo-alt.png" />
        </div>
        <h2 class="text-center">Acceso al Sistema</h2>
        <div class="form-group">
            <input type="email" runat="server" id="email" name="email" class="form-control" placeholder="Usuario" required="required" autofocus="autofocus" />
        </div>
        <div class="form-group">
            <input type="password" runat="server" id="password" name="password" class="form-control" placeholder="Contraseña" required="required" autofocus="autofocus" />
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-lg btn-block" runat="server" onserverclick="logeo_ServerClick" >Iniciar Session</button>
        </div>
    </div>
         </form>
</body>
</html>

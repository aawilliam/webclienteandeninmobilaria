﻿using CapaEntidad;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAndenInmobiliario
{
    public partial class WebProyecto : System.Web.UI.Page
    {
        string urlServicio = WebConfigurationManager.AppSettings["url_proyecto"];
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false) {
                vista.SetActiveView(listar);
                Listado();
            }
        }

        private void Listado() {

            var client = new RestClient(urlServicio);
            var request = new RestRequest("Proyecto");
            request.Method = Method.GET;
            IRestResponse<List<ProyectoDominio>> response = client.Execute<List<ProyectoDominio>>(request);
            List<ProyectoDominio>  listado = response.Data;
            gvProyectos.DataSource = listado;
            gvProyectos.DataBind();
        }

        protected void gvProyectos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
        }

        protected void grabar_ServerClick(object sender, EventArgs e)
        {
            if (proyect_codigo.Value.ToString() == "")
            {
                ProyectoDominio proyecto = new ProyectoDominio();
                proyecto.Nombre = proyect_nombre.Value.Trim();
                proyecto.Direccion = proyect_direccion.Value.Trim();
                proyecto.Latitud = proyect_latitud.Value.Trim();
                proyecto.Longitud = proyect_longitud.Value.Trim();
                proyecto.IdProyecto = 0;
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Proyecto");
                request.Method = Method.POST;
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddBody(proyecto);
                IRestResponse<ProyectoDominio> response = client.Execute<ProyectoDominio>(request);
                if (response.ErrorException != null)
                {
                    const string message = "ERROR: ";
                    var twilioException = new ApplicationException(message, response.ErrorException);
                    string myStringVariable = twilioException.InnerException.Message;
                    ClientScript.RegisterStartupScript(this.GetType(), "ERROR", "alert('" + myStringVariable + "');", true);

                }

            }
            else {
                ProyectoDominio proyecto = new ProyectoDominio();
                proyecto.Nombre = proyect_nombre.Value.Trim();
                proyecto.Direccion = proyect_direccion.Value.Trim();
                proyecto.Latitud = proyect_latitud.Value.Trim();
                proyecto.Longitud = proyect_longitud.Value.Trim();
                proyecto.IdProyecto = Convert.ToInt32(proyect_codigo.Value);
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Proyecto");
                var logeo = SimpleJson.SerializeObject(proyecto);
                request.Method = Method.PUT;
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddBody(proyecto);
                IRestResponse<ProyectoDominio> response = client.Execute<ProyectoDominio>(request);

                const string message = "ERROR: ";
                var twilioException = new ApplicationException(message, response.ErrorException);

                string myStringVariable = twilioException.InnerException.Message;
                ClientScript.RegisterStartupScript(this.GetType(), "ERROR", "alert('" + myStringVariable + "');", true);

               // throw twilioException;
            }

            this.Listado();
            vista.SetActiveView(listar);
        }

        protected void nuevo_Click(object sender, EventArgs e)
        {
            vista.SetActiveView(actualizar);
        }

        protected void gvProyectos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete") {

                int idProyecto = Convert.ToInt32(e.CommandArgument.ToString());
                //int prodcutid = Convert.ToInt32(lbtnDelete.CommandArgument.ToString());
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Proyecto/" + idProyecto.ToString(), Method.DELETE);
                IRestResponse response = client.Execute(request);
                this.Listado();


            }else if (e.CommandName == "Editar") {

                int idProyecto = Convert.ToInt32(e.CommandArgument.ToString());

                var client = new RestClient(urlServicio);
                var request = new RestRequest("Proyecto/"+idProyecto.ToString());
                request.Method = Method.GET;
                IRestResponse<ProyectoDominio> response = client.Execute<ProyectoDominio>(request);
                ProyectoDominio obj = response.Data;
                proyect_nombre.Value = obj.Nombre;
                proyect_direccion.Value = obj.Direccion;
                proyect_latitud.Value = obj.Latitud;
                proyect_longitud.Value = obj.Longitud;
                proyect_codigo.Value = obj.IdProyecto.ToString();
                ckBactivo.Checked = true;
                vista.SetActiveView(actualizar);
            }
        }
    }
}
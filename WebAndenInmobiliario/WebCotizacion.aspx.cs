﻿using CapaEntidad;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAndenInmobiliario
{
    public partial class WebCotizacion : System.Web.UI.Page
    {
        string urlServicio = WebConfigurationManager.AppSettings["url_cotizacion"];
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                vista.SetActiveView(listar);
                Listado();
            }
        }

        private void Listado()
        {
            var client = new RestClient(urlServicio);
            var request = new RestRequest("Cotizacion");
            request.Method = Method.GET;
            IRestResponse<List<CotizacionDominio>> response = client.Execute<List<CotizacionDominio>>(request);
            List<CotizacionDominio> listado = response.Data;
            gvCotizacion.DataSource = listado;
            gvCotizacion.DataBind();
        }

        protected void gvCotizacion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void grabar_ServerClick(object sender, EventArgs e)
        {
            if (idCotizacion.Value.ToString()!= "")
            {
                CotizacionDominio cotizacion = new CotizacionDominio();
                cotizacion.idCotizacion = Convert.ToInt32(idCotizacion.Value.Trim());
                cotizacion.Dni = dni.Value.ToString();
                cotizacion.Nombres = nombres.Value.ToString().Trim();
                cotizacion.Apellidos = apellidos.Value.ToString();
                cotizacion.Correo = correo.Value.ToString();
                cotizacion.Telefono = telefono.Value.ToString();
                cotizacion.Estado = true;
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Cotizacion");
                request.Method = Method.PUT;
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddBody(cotizacion);
                IRestResponse<CotizacionDominio> response = client.Execute<CotizacionDominio>(request);
                if (response.ErrorException != null)
                {
                    const string message = "ERROR: ";
                    var twilioException = new ApplicationException(message, response.ErrorException);
                    string myStringVariable = twilioException.InnerException.Message;
                    ClientScript.RegisterStartupScript(this.GetType(), "ERROR", "alert('" + myStringVariable + "');", true);

                }
            }

            this.Listado();
            vista.SetActiveView(listar);
        }

        protected void nuevo_Click(object sender, EventArgs e)
        {
            vista.SetActiveView(actualizar);
        }

        protected void gvCotizacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {

                int idCotizacion = Convert.ToInt32(e.CommandArgument.ToString());
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Cotizacion/" + idCotizacion.ToString(), Method.DELETE);
                IRestResponse response = client.Execute(request);
                this.Listado();
            }
            else if (e.CommandName == "Editar")
            {
                int idProyecto = Convert.ToInt32(e.CommandArgument.ToString());
                var client = new RestClient(urlServicio);
                var request = new RestRequest("Cotizacion/" + idProyecto.ToString());
                request.Method = Method.GET;
                IRestResponse<CotizacionDominio> response = client.Execute<CotizacionDominio>(request);
                CotizacionDominio obj = response.Data;
                idCotizacion.Value = Convert.ToString(obj.idCotizacion);
                dni.Value = obj.Dni;
                nombres.Value = obj.Nombres;
                apellidos.Value = obj.Apellidos;
                correo.Value = obj.Correo;
                telefono.Value = obj.Telefono;
                vista.SetActiveView(actualizar);
            }
        }
    }
}
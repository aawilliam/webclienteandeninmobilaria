﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class CotizacionDAO
    {

        public bool Registrar(Cotizacion obj)
        {
            try
            {
                bool bResultado = false;
                ServiceInmobiliaria.Cotizacion cotizacion = new ServiceInmobiliaria.Cotizacion();
                cotizacion.Dni = obj.dni;
                cotizacion.Apellidos = obj.apellidos;
                cotizacion.Correo = obj.correo;
                cotizacion.Nombres = obj.nombres;
                cotizacion.Telefono = obj.telefono;
                ServiceInmobiliaria.ServiceCotizacionClient wcfInmobiliaria = new ServiceInmobiliaria.ServiceCotizacionClient();

                if (wcfInmobiliaria.RegistrarCotizacion(cotizacion) != null)
                {
                    bResultado = true;
                }

                return bResultado;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class UsuarioDominio
    {
       
        public int IdUsuario { get; set; }
   
        public string Dni { get; set; }
  
        public string Nombre { get; set; }
 
        public string Apellidos { get; set; }

        public int IdRol { get; set; }

        public string Email { get; set; }

        public bool Activo { get; set; }

        public string Clave { get; set; }

    }
}

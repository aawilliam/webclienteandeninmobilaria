﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class ProyectoDominio
    {      
        public int IdProyecto { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; } 
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}

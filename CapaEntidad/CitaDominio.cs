﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class CitaDominio
    {
        
        public int idCita { get; set; }
      
        public string fechaCita { get; set; }
      
        public int idSolicitud { get; set; }
      
        public int idUsuario { get; set; }
       
        public bool estado { get; set; }

    }
}
